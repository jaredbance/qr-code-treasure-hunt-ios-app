//
//  SignUpController.swift
//  scanner_v3
//
//  Created by Jared Bance on 2018-09-23.
//  Copyright © 2018 Jared Bance. All rights reserved.
//

import UIKit
import Firebase

/// Controller for SignUp View
class SignUpController: UIViewController {
    
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var createAccountBtn: UIButton!
    
    var db: Firestore!
    

    /// Users taps creatAccountBtn after inputing account info
    ///
    /// - Parameter sender:
    @IBAction func createAccountBtnTapped(_ sender: UIButton) {
        self.createAccountBtn.isEnabled = false
        self.usernameField.isEnabled = false
        self.passwordField.isEnabled = false
        let email = usernameField.text
        let password = passwordField.text
        
        Auth.auth().createUser(withEmail: email!, password: password!, completion: { (user, error) in
            if let u = user{
                
                Auth.auth().signIn(withEmail: email!, password: password!) { (user, error) in
                    if user != nil {
                        print("LOGGED")
                        let uid = Auth.auth().currentUser?.uid
                        let testString = "LOGGED " + uid!
                        print(testString)
                        
                        // Create docuemnts in database for new user
                        // add user to database list of users
                        self.db.collection("Users").document(uid!).setData([
                            "Name": uid?.suffix(8) as Any,
                            ]) { err in
                                if let err = err {
                                    print("Error writing document: \(err)")
                                } else {
                                    print("Document successfully written!")
                                }
                        }
                        
                        self.db.collection("Users").document(uid!).collection("Scanned QR Codes").document("init").setData([
                            "Score": 0,
                            ]) { err in
                                if let err = err {
                                    print("Error writing document: \(err)")
                                } else {
                                    print("Document successfully written!")
                                }
                        }
                        
                        // add user to leaderboard database
                        self.db.collection("Leaderboard").document(uid!).setData([
                            "Name": uid?.suffix(8) as Any,
                            "Score": 0
                        ]) { err in
                            if let err = err {
                                print("Error writing document: \(err)")
                            } else {
                                print("Document successfully written!")
                            }
                        }
                        
                    }
                    else {
                        if let myError = error?.localizedDescription {
                            print(myError)
                        }
                        else {
                            print ("ERROR")
                        }
                    }
                }
            
                let alert = UIAlertController(title: "Account Creating Succesful", message: "You can now login on the main screen", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
                
            }
            else {
                print(error as Any)
                let errorString = error?.localizedDescription
                let alert = UIAlertController(title: "Account Creation Failed", message: errorString, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
                self.createAccountBtn.isEnabled = true
                self.usernameField.isEnabled = true
                self.passwordField.isEnabled = true
                return
            }
            
        })
        
        try! Auth.auth().signOut()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //////////////////////// FIRESTORE STUFF
        // [START setup]
        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        // [END setup]
        db = Firestore.firestore()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Hide keyboard when user touches outside keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Hide keyboard when user touches return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        usernameField.resignFirstResponder()
        return (true)
    }

}

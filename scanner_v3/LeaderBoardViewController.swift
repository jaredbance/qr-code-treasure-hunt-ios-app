//
//  LeaderBoardViewController.swift
//  scanner_v3
//
//  Created by Jared Bance on 2018-09-23.
//  Copyright © 2018 Jared Bance. All rights reserved.
//

import UIKit
import Firebase

/// Controller for the leaderboard view
class LeaderBoardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet var leaderBoardTableView: UITableView! // table to contain the leaderboard
    
    var scoreList = [String]() // list of user's scores
    var namesList = [String]() // list of user's names
    
    var numOfRanks = 25 // number of users shown on Leaderboard
    
    /// This fucntion sets the length of the table view of the Leaderboard
    ///
    /// - Parameters:
    ///   - tableView:
    ///   - section:
    /// - Returns:
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return(scoreList.count)
    }
    
    /// Takes data from scoreList and namesList and puts them in cells in the leaderboard table
    ///
    /// - Parameters:
    ///   - tableView:
    ///   - indexPath:
    /// - Returns: 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellString: String
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        
        cellString = String(indexPath.row + 1) + ". " + namesList[indexPath.row] + ": " + scoreList[indexPath.row] + " points"
        cell.textLabel?.text = cellString
        return cell
        
        
    }
    
    var db: Firestore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // [START setup]
        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        // [END setup]
        db = Firestore.firestore()
        
        // create arrays containing leaderboard data
        db.collection("Leaderboard").order(by: "Score", descending: true).limit(to: numOfRanks).getDocuments() { (querySnapshot, err) in
            var scoreString: String = "error"
            var nameString: String = "error"
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    if let val = document.get("Score") {
                        scoreString = (val as AnyObject).stringValue
                    }
                    nameString = document.get("Name") as! String
                    self.scoreList.append(scoreString)
                    self.namesList.append(nameString)
                }
                self.leaderBoardTableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

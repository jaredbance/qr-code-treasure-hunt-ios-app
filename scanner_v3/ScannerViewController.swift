//
//  ScannerViewController.swift
//  scanner_v3
//
//  Created by Jared Bance on 2018-08-23.
//  Copyright © 2018 Jared Bance. All rights reserved.
//

import AVFoundation
import UIKit
import Firebase

/// Controller for Scanner View
/// Users scan QR codes in this view
class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var btnBack: UIButton!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var codeExists: Bool!
    var alertMsg: String! = "NULL" // alert message to send to ViewController upon segue
    
    var db: Firestore!
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        self.view.bringSubview(toFront: btnBack)
        
        captureSession.startRunning()
        
        ///////////////////////// FIRESTORE STUFF
        // [START setup]
        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        // [END setup]
        db = Firestore.firestore()
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        //dismiss(animated: true)
    }
    
    func found(code: String) {
        print(code)
        let docRef = db.collection("QR Codes").document(code)
        
        // Check if code exists in database
        docRef.getDocument {(document, error) in
            if let document = document, document.exists { // Code exists in database
                var dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                dataDescription = dataDescription.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression) // Remove non integer characters from
                                                                                                                            // retrieved data Description String
                let score: Int = Int(dataDescription)!
                print(score)
                
                print("FOUND!!!")
                
                var uid: String
                
                // get user uid
                let user = Auth.auth().currentUser
                if let user = user {
                    uid = user.uid
                }
                uid = (user?.uid)!
                
                
                // check users account to see if they have already scanned the code before
                let docRef2 = self.db.collection("Users").document(uid).collection("Scanned QR Codes").document(code)
                docRef2.getDocument { (document, error) in
                    if let document = document, document.exists {
                        print("Code is scanned by Users Account")
                        self.alertMsg = "already scanned"
                        self.performSegue(withIdentifier: "ScannerToMainMenu", sender: self)
                    } else {
                        print("User hasnt scanned this code before")
                        
                        // add code to users account
                        self.db.collection("Users").document(uid).collection("Scanned QR Codes").document(code).setData([
                            "Score": score,
                            ]) { err in
                                if let err = err {
                                    print("Error writing document: \(err)")
                                } else {
                                    print("Document successfully written!")
                                }
                        }
                        self.updateLeaderboard()
                        self.alertMsg = "new code"
                        self.performSegue(withIdentifier: "ScannerToMainMenu", sender: self)
                    }
                }
            } else { // code wasnt in database
                print("Code wasnt in database")
                self.alertMsg = "invalid code"
                self.performSegue(withIdentifier: "ScannerToMainMenu", sender: self)
            }
            //self.performSegue(withIdentifier: "ScannerToMainMenu", sender: self)
        }
    }
    
    /// update the leaderboard by adding updating leaderboard files in database
    func updateLeaderboard(){
        print("updating leaderboard")
        // get user ID
        var uid: String
        let user = Auth.auth().currentUser
        if let user = user {
            uid = user.uid
        }
        uid = (user?.uid)!
        
        // Calculate users total score by adding up scores of each code scanned
        db.collection("Users").document(uid).collection("Scanned QR Codes").getDocuments() { (querySnapshot, err) in
            var totalScore: Int = 0
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    totalScore += document.get("Score") as! Int
                }
                print(totalScore)
                
                self.db.collection("Leaderboard").document(uid).updateData([
                    "Score": totalScore,
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        print("Document successfully updated")
                    }
                }
            }
        }

    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    // tell main menu what type of alert to display upon returing
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is ViewController
        {
            let vc = segue.destination as? ViewController
            vc?.updateAlert(alert: self.alertMsg)
           // vc?.alertType = self.alertMsg
            self.alertMsg = "NULL" // reset alert message
        }
    }
    
}

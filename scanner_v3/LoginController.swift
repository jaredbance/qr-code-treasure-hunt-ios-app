//
//  LoginController.swift
//  scanner_v3
//
//  Created by Jared Bance on 2018-08-28.
//  Copyright © 2018 Jared Bance. All rights reserved.
//

import UIKit
import Firebase
//import GoogleSignIn

/// Controller for login screen view
class LoginController: UIViewController, UITextFieldDelegate {
    
    var handle: AuthStateDidChangeListenerHandle?
    public var ID: String!
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var loadingInd: UIActivityIndicatorView!
    
    @IBAction func loginButton(_ sender: UIButton) {
        self.loadingInd.startAnimating()
                if usernameField.text != "" && passwordField.text != "" {
                    Auth.auth().signIn(withEmail: usernameField.text!, password: passwordField.text!) { (user, error) in
                        if user != nil {
                            self.performSegue(withIdentifier: "loginSegue", sender: self)
                            self.ID = Auth.auth().currentUser?.uid
                        }
                        else {
                            if let myError = error?.localizedDescription {
                                print(myError)
                                self.loadingInd.stopAnimating()
                            }
                            else {
                                print ("ERROR")
                                self.loadingInd.stopAnimating()
                            }
                        }
                    }
                }
                else {
                    self.loadingInd.stopAnimating()
                }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Hide keyboard when user touches outside keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /// Hide keyboard when user touches return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        usernameField.resignFirstResponder()
        return (true)
    }
    
    /// Get's users id and returns it
    ///
    /// - Returns: the ID
    public func getID() -> String {
        return self.ID
    }
    
}

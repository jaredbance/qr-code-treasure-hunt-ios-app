//
//  loggedOut.swift
//  scanner_v3
//
//  Created by Jared Bance on 2018-09-03.
//  Copyright © 2018 Jared Bance. All rights reserved.
//

import UIKit

/// Controller for first screen view
class loggedOut: UIViewController {
    
    
    
    @IBAction func openLoginView(_ sender: UIButton) {
        self.performSegue(withIdentifier: "loginScreenSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

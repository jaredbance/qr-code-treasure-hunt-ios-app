//
//  ViewController.swift
//  scanner_v3
//
//  Created by Jared Bance on 2018-08-23.
//  Copyright © 2018 Jared Bance. All rights reserved.
//

import UIKit
import Firebase

/// Controller for main view
class ViewController: UIViewController {
    
    var db: Firestore!
    var alertType: String! = "NULL" // check each time view is loaded to see if alert needs to be displayed
    
    
    @IBOutlet var nameBtn: UIButton!
    
    /// Logout when logout button tapped
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func logout(_ sender: UIButton) {
        try! Auth.auth().signOut()
        performSegue(withIdentifier: "logoutSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // [START setup]
        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        // [END setup]
        db = Firestore.firestore()
        
        // get uid
        let uid = Auth.auth().currentUser?.uid
        
        // update label on nameBtn with users name
        let docRef2 = self.db.collection("Users").document(uid!)
        docRef2.getDocument { (document, error) in
            if let document = document, document.exists {
                let nameString = "Name: " + (document.get("Name") as? String)!
                self.nameBtn.setTitle(nameString, for: .normal)
            } else {
                print("Error getting name field")
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // check if need to display an alert
        if alertType != "NULL"{
            let alert = UIAlertController(title: "null", message: "null", preferredStyle: .alert)
            print("\n\nALERT " + alertType + "\n\n")
            switch alertType {
            case "invalid code":
                alert.title = "Error"
                alert.message = "This QR code is invalid"
            case "already scanned":
                alert.title = "Error"
                alert.message = "You've already scanned this code before"
            case "new code":
                alert.title = "Success"
                alert.message = "You've found a new code!"
            case .none:
                print("some other non-nil status")
            case .some(_):
                print("status was nil...")
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true) // display alert
            updateAlert(alert: "NULL") // reset alert
        }
    }
    
    func updateAlert(alert: String){
        alertType = alert
    }



}

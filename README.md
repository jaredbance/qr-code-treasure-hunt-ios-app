# QR Code Treasure Hunt IOS APP

Users of this app can scan QR codes hidden around the University of Calgary Campus.
Each QR code scanned adds to the users individual score. 
Each user has their own account which contain the users score. 
Users can see who has the most points by viewing the in-app leaderboard.

# Demonstration Use Case Video
https://youtu.be/tstjU-Nc5H8

## Authors

* **Jared Bance**
